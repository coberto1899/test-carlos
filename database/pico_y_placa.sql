-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: May 09, 2021 at 05:09 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pico_y_placa`
--

-- --------------------------------------------------------

--
-- Table structure for table `week_day_times`
--

DROP TABLE IF EXISTS `week_day_times`;
CREATE TABLE IF NOT EXISTS `week_day_times` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `week_day` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plate` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `begin_time` time NOT NULL,
  `end_time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `week_day_times`
--

INSERT INTO `week_day_times` (`id`, `week_day`, `plate`, `begin_time`, `end_time`, `created_at`, `updated_at`) VALUES
(1, '1', '1', '07:00:00', '09:30:00', NULL, NULL),
(2, '1', '2', '07:00:00', '09:30:00', NULL, NULL),
(3, '1', '1', '16:00:00', '19:30:00', NULL, NULL),
(4, '1', '2', '16:00:00', '19:30:00', NULL, NULL),
(5, '2', '3', '07:00:00', '09:30:00', NULL, NULL),
(6, '2', '4', '07:00:00', '09:30:00', NULL, NULL),
(7, '2', '3', '16:00:00', '19:30:00', NULL, NULL),
(8, '2', '4', '16:00:00', '19:30:00', NULL, NULL),
(9, '3', '5', '07:00:00', '09:30:00', NULL, NULL),
(10, '3', '6', '07:00:00', '09:30:00', NULL, NULL),
(11, '3', '5', '16:00:00', '19:30:00', NULL, NULL),
(12, '3', '6', '16:00:00', '19:30:00', NULL, NULL),
(13, '4', '7', '07:00:00', '09:30:00', NULL, NULL),
(14, '4', '8', '07:00:00', '09:30:00', NULL, NULL),
(15, '4', '7', '16:00:00', '19:30:00', NULL, NULL),
(16, '4', '8', '16:00:00', '19:30:00', NULL, NULL),
(17, '5', '9', '07:00:00', '09:30:00', NULL, NULL),
(18, '5', '0', '07:00:00', '09:30:00', NULL, NULL),
(19, '5', '9', '16:00:00', '19:30:00', NULL, NULL),
(20, '5', '0', '16:00:00', '19:30:00', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
