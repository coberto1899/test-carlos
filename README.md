# README #

This is a "Pico y Placa" validator, it validates if your vehicle can circulate based on date and time.

### What is this repository for? ###

* Laravel version 8.12
* PHP 7.4
* Bootstrap 4a

### How do I get set up? ###

* There's an SQL file with the BBDD needed for the validations, you can create the BBDD using the migrate function or you can restore the SQL.
* You must set up the .env file with your localhost credentials.

### Who do I talk to? ###

* Repo owner or admin
* GOOD LUCK!!
