<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//PICO Y PLACA ROUTES
Route::get('/', 'App\Http\Controllers\picoYPlacaController@index');
Route::post('/calculate/date', 'App\Http\Controllers\picoYPlacaController@calculateDate');
Route::get('/fullcaledar/get/{parameter}', 'App\Http\Controllers\picoYPlacaController@getFullCalendar');
