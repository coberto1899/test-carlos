<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">

        <title>{{ config('app.name', 'Laravel') }}</title>
        <link rel="shortcut icon" href="{{asset('images/icon.png')}}">

        <!-- Scripts -->
        <script src="{{ asset('js/jquery/jquery.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/general.js') }}"></script>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/general.css') }}" rel="stylesheet">
        <script type="text/javascript">
        function load() {
            var loaderGif = document.getElementById("loaderGif");
            loaderGif.classList.remove("loaderGif");
            var loaderBody = document.getElementById("loaderBody");
            loaderBody.classList.remove("loaderBody");
        }
        window.onload = load;
        </script>
    </head>

    <body>
        <div id='loaderGif' class="loaderGif"></div>
        <div id="loaderBody" class="loaderBody"></div>
        <div id="wrapper" class="">
            <!-- Just an image -->
            <nav class="navbar navbar-light bg-light customNav">
              <a class="navbar-brand" href="#">
                <img src="{{asset('images/logo.png')}}" width="100%" height="30" alt="">
              </a>
            </nav>

            <div id="content" class="">
                @yield('content')
            </div>

        </div>
    </body>
</html>