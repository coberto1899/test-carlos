@extends('layout.app')

@section('content')
<link href="{{ asset('css/fullcalendar/main.css') }}" rel="stylesheet">
<script src="{{ asset('js/fullcalendar/main.js') }}"></script>

<div class="container-fluid">
    <div class="col-md-10 offset-md-1 border">
        <div class="col-md-12">
            <form id="calculateForm" method="POST" action="{{asset('/channel/store')}}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-4">
                        <label for="plate">Placa:</label>
                        <input class="form-control" type="text" id="plate" name="plate" placeholder="Placa" required="required" onchange="removeInputRedFocus(this.id)"> 
                    </div>
                    <div class="col-md-4">
                        <label for="date">Fecha:</label>
                        <input class="form-control" type="date" id="date" name="date" required="required" onchange="removeInputRedFocus(this.id)"> 
                    </div>
                    <div class="col-md-4">
                        <label for="time">Hora:</label>
                        <input class="form-control" type="time" id="time" name="time" required="required" onchange="removeInputRedFocus(this.id)"> 
                    </div>
                </div>
                <div class="row actionsBtn">
                    <div class="col-md-1">
                        <a class="btn btn-light btnCancel" align="left" href="#" onclick="clearForm()"> Limpiar </a>
                    </div>
                    <div class="col-md-10 offset-md-1">
                        <button type="submit" class="btn btn-primary btnSubmit" align="right" onclick="submitForm()"> Buscar </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="resultDiv" class="col-md-10 offset-md-1 border hidden resultDiv">
        <div class="col-md-12">
            <center id="resultDivBody">
            </center>
        </div>
    </div>
</div>
<button id="myModalFullCalendarBtn" type="button" class="btn btn-info btn-lg hidden" data-toggle="modal" data-target="#myModalFullCalendar">Open Modal</button>
<!-- Modal -->
<div id="myModalFullCalendar" class="modal fade hidden" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content" style="height:600px !important">
            <div class="modal-header">
                <h4 class="modal-title">Calendario</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div id="calendar" class="modal-body" style="height:600px !important">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>
<script src="{{ asset('js/picoYPlaca/index.js') }}"></script>
@endsection