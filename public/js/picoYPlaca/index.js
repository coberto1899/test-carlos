/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
});

function clearForm(){
    document.getElementById("plate").value = '';
    document.getElementById("date").value = '';
    document.getElementById("time").value = '';
}

function submitForm() {
    event.preventDefault();
    var resultDivBody = document.getElementById("resultDiv"); 
    $(resultDivBody).addClass('hidden');
    
    //Validate Inputs
    var validate = false;
    var plate = document.getElementById("plate"); if(plate.value === ''){ $(plate).addClass('inputRedFocus'); validate = true; }else{ $(plate).removeClass('inputRedFocus'); }
    var date = document.getElementById("date"); if(date.value === ''){ $(date).addClass('inputRedFocus'); validate = true; }else{ $(date).removeClass('inputRedFocus'); }
    var time = document.getElementById("time"); if(time.value === ''){ $(time).addClass('inputRedFocus'); validate = true; }else{ $(time).removeClass('inputRedFocus'); }
    
    if(validate === false){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        var form = document.getElementById('calculateForm');
        $.ajax({
            url: 'calculate/date',
            type: "POST",
            data: new FormData(form),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                // Show Loader
                $("#loaderGif").addClass('loaderGif');
            },
            success: function (data)
            {
                //Show Result Div
                var resultDiv = document.getElementById('resultDiv');
                $(resultDiv).removeClass('hidden');
                document.getElementById('resultDivBody').innerHTML = '' ;
                if(data['success'] == 'error'){
                    document.getElementById('resultDivBody').innerHTML = data['Error'] ;
                }
                if(data['success'] == 'true'){
                    document.getElementById('resultDivBody').innerHTML = data['Error'] ;
                }
            },
            complete: function () {
                //Hide Loader
                var loaderGif = document.getElementById("loaderGif");
                loaderGif.classList.remove("loaderGif");
            }
        });    
    }
}

function fullCalendar(plate, date){
    document.getElementById("calendar").innerHTML = ""; 
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        headerToolbar: {
            left: 'prev,next',
            center: 'title',
            right: 'timeGridWeek'
        },
        initialView: 'timeGridWeek',
        initialDate: date,
        allDaySlot: false,  
        slotMinTime: '05:00:00', /* calendar start Timing */
        slotMaxTime: '20:00:00',  /* calendar end Timing */
        editable: false,
        navLinks: false, // can click day/week names to navigate views
        dayMaxEvents: false, // allow "more" link when too many events
        events: {
            url: 'fullcaledar/get/'+plate,
            failure: function () {
                document.getElementById('script-warning').style.display = 'block';
            }
        },
        loading: function (bool) {
            if(bool === true){ $("#loaderGif").addClass('loaderGif'); }else{ $("#loaderGif").removeClass('loaderGif'); }
        }
    });

    document.getElementById("myModalFullCalendarBtn").click();
    calendar.render();
}