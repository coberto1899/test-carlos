<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class weekDayTimes extends Model
{
    use HasFactory;
    
    protected $table = 'week_day_times';
}
