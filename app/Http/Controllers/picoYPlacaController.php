<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Event;
use DB;

class picoYPlacaController extends Controller
{
    public function index() {
        return view('picoYPlaca.index');
    }

    public function calculateDate(request $request){
        $plate = $request['plate'];
        $date = $request['date'];
        $time = $request['time'];
        //Validate if valid plate
        $validatePlate = validatePlateANT($plate);
        if($validatePlate['success'] == 'error'){
            return $validatePlate;
        }
        
        //Validate if valid DateTime
        $validateDateTime = validateDateTime($request['date'], $request['time']);
        if($validateDateTime == '1970-01-01 12:00:00 AM UTC'){
            $returnData = [
                'Error' => "<div class='alert alert-danger'><center style='font-size:14px'>El vehiculo ingresado no puede circular en la fecha y hora indicada.</center></div>",
                'success' => 'error'
            ];
            return $returnData;
        }
        
        //Validate if it can circulate
        $weekEnd = isWeekend($request['date']);
        
        if($weekEnd){
            $returnData = [
                'Error' => "<div class='alert alert-success'><center style='font-size:14px'>El vehiculo ingresado si puede circular en la fecha y hora indicada.<br>Adicionalmente, en el siguiente <a href='#' onclick='fullCalendar(\"$plate\", \"$date\")'>enlace</a> puede ver el calendario de Pico y Placa segun su vehiculo.</center></div>",
                'success' => 'error'
            ];
            return $returnData;
        }else{
            //Check in BBDD
            $plateLength = strlen($plate);
            $plateString = str_split($plate);
            $weekDay = getWeekday($date);
            $weekDayTimeSearch = \App\Models\weekDayTimes::where('plate','=',$plateString[$plateLength-1])
                                                        ->where('week_day','=',$weekDay)
                                                        ->where('begin_time','<=',$time)
                                                        ->where('end_time','>=',$validateDateTime)
                                                        ->get();
            if($weekDayTimeSearch->isEmpty()){
                $returnData = [
                    'Error' => "<div class='alert alert-success'><center style='font-size:14px'>El vehiculo ingresado si puede circular en la fecha y hora indicada.<br>Adicionalmente, en el siguiente <a href='#' onclick='fullCalendar(\"$plate\", \"$date\")'>enlace</a> puede ver el calendario de Pico y Placa segun su vehiculo.</center></div>",
                    'success' => 'error'
                ];
            }else{
                $returnData = [
                    'Error' => "<div class='alert alert-danger'><center style='font-size:14px'>El vehiculo ingresado no puede circular en la fecha y hora indicada.<br>Adicionalmente, en el siguiente <a href='#' onclick='fullCalendar(\"$plate\", \"$date\")'>enlace</a> puede ver el calendario de Pico y Placa segun su vehiculo.</center></div>",
                    'success' => 'error'
                ];
            }
            return $returnData;
        }
    }
    
    public function getFullCalendar($plate) {
        // Short-circuit if the client did not give us a date range.
        if (!isset($_GET['start']) || !isset($_GET['end'])) {
          die("Please provide a date range.");
        }

        // Parse the start/end parameters.
        // These are assumed to be ISO8601 strings with no time nor timeZone, like "2013-12-29".
        // Since no timeZone will be present, they will parsed as UTC.
        $range_start = parseDateTime($_GET['start']);
        $range_end = parseDateTime($_GET['end']);

        // Parse the timeZone parameter if it is present.
        $time_zone = null;
        if (isset($_GET['timeZone'])) {
          $time_zone = new DateTimeZone($_GET['timeZone']);
        }
        
        //Check in BBDD
        $plateLength = strlen($plate);
        $plateString = str_split($plate);
        $dates = displayDates($_GET['start'], $_GET['end']);
        $weekDays = array();
        foreach($dates as $d){
            $day = getWeekday($d);
            $weekDays[] = array(
                'date' => $d,
                'day' => $day
            );
        }
        $weekDayTimeSearch = \App\Models\weekDayTimes::where('plate','=',$plateString[$plateLength-1])
                                                    ->whereIn('week_day',[1,2,3,4,5])
                                                    ->get();
        
        $data = array();
        foreach ($weekDayTimeSearch as $detail) {
            foreach($weekDays as $w){
                if($w['day'] == $detail->week_day){
                    $data[] = array(
                        'id' => $detail->id,
                        'title' => 'No pueden circular placas terminadas en: '.$detail->plate,
                        'start' => $w['date'] .' '. $detail->begin_time,
                        'end' => $w['date'] .' '.$detail->end_time,
                        'color' => "red"
                    );
                }
            }
        }

        // Send JSON to the client.
        echo json_encode($data);
    }
}
