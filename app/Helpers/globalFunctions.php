<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function validatePlateANT($plate) {
    header("Content-Type: application/json");
    $plate = strtoupper($plate);
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0", // don't return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_ENCODING => "UTF8", // handle all encodings
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 5, // timeout on connect
        CURLOPT_TIMEOUT => 10, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
        CURLOPT_SSL_VERIFYPEER => false, // Quita SSL
    );

//    $c = curl_init('http://consultas.atm.gob.ec/PortalWEB/paginas/clientes/clp_grid_citaciones.jsp?ps_tipo_identificacion=PLA&ps_identificacion='.$plate.'&ps_placa=' . $plate . '&ps_placa=');
    $c = curl_init('https://sistematransito.ant.gob.ec:5038/PortalWEB/paginas/clientes/clp_grid_citaciones.jsp?ps_tipo_identificacion=PLA&ps_identificacion=' . $plate . '&ps_placa=');

    curl_setopt_array($c, $options);
    $page = utf8_decode(curl_exec($c));
    $error = curl_error($c);
    curl_close($c);
    
    preg_match_all('|<[^>]+>(.*)</[^>]+>|U', $page, $resultado);
//    print_r($resultado[1]);die();
    if ($error) { //CURL ERROR - CONNECTION ERROR
        $respuesta = array(
            "Error" => "<div class='alert alert-danger'><center style='font-size:14px'>La placa ($plate) ingresada es incorrecta, por favor ingrese una placa valida.</center></div>",
            "success" => "antDown"
        );
    } else {
        if (isset($resultado[1][13])) { //VALID PLATE
            http_response_code(200);
            $respuesta = array(
                "placa" => $plate,
                "marca" => $resultado[1][15],
                "color" => $resultado[1][17],
                "anioMatricula" => $resultado[1][19],
                "modelo" => $resultado[1][22],
                "clase" => $resultado[1][24],
                "fechaMatricula" => $resultado[1][26],
                "anioFabricacion" => $resultado[1][28],
                "servicio" => $resultado[1][30],
                "fechaCaducidad" => $resultado[1][32],
                "success" => "true"
            );
        } else if (!isset($resultado[0][13])) { // INVALID PLATE
            http_response_code(400);
            $respuesta = array(
                "Error" => "<div class='alert alert-danger'><center style='font-size:14px'>La placa ($plate) ingresada es incorrecta, por favor ingrese una placa valida</center></div>",
                "success" => "antDown"
            );
        } else { // ANT ERROR - WEBSITE DOWN
            $respuesta = array(
                "Error" => "<div class='alert alert-danger'><center style='font-size:14px'>La placa ($plate) ingresada es incorrecta, por favor ingrese una placa valida.</center></div>",
                "success" => "error"
            );
        }
    }
    return $respuesta;
}

function isWeekend($dt) {
    $dt1 = strtotime($dt);
    $dt2 = date("l", $dt1);
    $dt3 = strtolower($dt2);
    if (($dt3 == "saturday" ) || ($dt3 == "sunday")) {
        return true;
    } else {
        return false;
    }
}

function validateDateTime($date, $time){
    $string = $date.' '.$time;
    $dateStr = strtotime($string);
    return date("Y-m-d H:i:s",$dateStr);
}

function displayDates($date1, $date2, $format = 'Y-m-d') {
    $dates = array();
    $current = strtotime($date1);
    $date2 = strtotime($date2);
    $stepVal = '+1 day';
    while ($current <= $date2) {
        $dates[] = date($format, $current);
        $current = strtotime($stepVal, $current);
    }
    return $dates;
}

function getWeekday($date) {
    return date('w', strtotime($date));
}
